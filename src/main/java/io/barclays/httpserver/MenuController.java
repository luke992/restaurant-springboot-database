package io.barclays.httpserver;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class MenuController {
    private MenuRepository repo;
    private RestaurantRepository restaurantRepo;

    public MenuController(MenuRepository repo, RestaurantRepository restaurantRepo) {
        this.repo = repo;
        this.restaurantRepo = restaurantRepo;
    }

    @PostMapping("/restaurants/{restaurant_id}/menus")
    public Menu addMenu(@RequestBody Menu menuData, @PathVariable Integer restaurant_id) {
        Restaurant restaurant = restaurantRepo.findById(restaurant_id).get();
        menuData.setRestaurant(restaurant);
        return repo.save(menuData);
    }

    @PutMapping("/restaurants/{restaurant_id}/menus/{id}")
    public Menu updateOne(@RequestBody Menu menuUpdate, @PathVariable Integer restaurant_id, @PathVariable Integer id) {
        Optional<Menu> optionalMenu = repo.findById(id);
        try {
            Menu menu = optionalMenu.get();
            menu.setTitle(menuUpdate.getTitle());

            return repo.save(menu);

        } catch (Exception err) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
        }
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{id}")
    public void deleteOne(@PathVariable Integer restaurant_id, @PathVariable Integer id) {
        repo.deleteById(id);
    }
}
