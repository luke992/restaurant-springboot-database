let data = []
const state = {
    selectedRestaurant: null,
    newMenu:null
}

fetch(`/restaurants`)
    .then(response => response.json())
    .then(_data => {
        data = _data
        render()
    })
    .catch(err => console.error(err))


    function updateMenuName(inputValue) {
        if (!state.newMenu) {
            state.newMenu = {
                name: "",
                items: []
            }
        }
    
        state.newMenu.name = inputValue
    }

function submitNewMenu(restaurant_id) {
    fetch(`/restaurants/${restaurant_id}/menus`, {
        method: 'POST',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify({title: state.newMenu.name})
    })
    .then(res => res.json())
    .then(menu => {
        menu, state.newMenu
        render()
    })
}
  





function getAddFormHTML() {
    return `
        <article>
        <form onsubmit="event.preventDefault();addRestaurant(this);">
        <label>Restaurant Name</label>
              <input name= "name" required/>
              <label>ImageURL</label>
              <input name= "imageURL" type="url" required />
              <button class="modal-button" style="width : 13rem;">Add Restaurant</button>
        </form>
        </article>
        `
}

function getUpdateFormHTML() {
    return `
        <article>
        <form onsubmit="event.preventDefault();updateRestaurant(this);">
              <input restaurant_id= "restaurant_id" required/>
              <label>update restaurant_id</label>
              <label>update Restaurant Name</label>
              <input name= "name" required/>
              <label>Update ImageURL</label>
              <input name= "ImageURL" type="url" required />
              <button class="modal-button" style="width : 13rem;">Update Restaurant(t)</button>
        </form>
        </article>
        `

}


function render() {
    let content = data.map((restaurantData, i) => {
        return `
        <article class ="restaurant-card">
            <div style="background-image: url('${restaurantData.imageURL}');"></div>
            <footer>
            <h2 id="${i}-header">${restaurantData.name}</h2>
            <button class="modal-button" onclick="displayMenus(${i})">Menus</button>
            <footer>
        </article>
        `
    }).join("")

    content += getAddFormHTML()


    const appEL = document.getElementById(`app`)
    appEL.innerHTML = content

    if (state.selectedRestaurant) {
        const modalContent = `
           <section class="modal-bg">
           <article>
           <main>
               ${state.selectedRestaurant.menus.map(menu => {
            return `
                   <article>
                   <h3>${menu.title}</h3>
                   <button class="accordion" onclick="openAccordian()">Items</button>
                   <div class="panel">
                   ${menu.items.map(item => {
                return `<p>${item.itemName} £${item.price}</p>
                       `
            })}
                   </div> 
                   </article>
                   `
        }).join("")}
                  </main>
                  <footer> 
               <button class="modal-button" onclick="loadCreateMenuForm(${state.selectedRestaurant.id})">Create Menu</button>
               <button class="modal-button" onclick="closeModal()">close</button>
               </footer>
               </article>
           </section>
        `
        const modalEL = document.getElementById(`modal`)
        modalEL.innerHTML = modalContent
    } else {
        const modalEL = document.getElementById(`modal`)
        modalEL.innerHTML = ""
    }
}

function loadCreateMenuForm(restaurant_id) {
    const createForm = `
    <form onsubmit="event.preventDefault();submitNewMenu(${restaurant_id})">
        <h3>Create a new Menu</h3>
        <input type="hidden" name="restaurant_id" value="${restaurant_id}" /> 
        <div>
            <label>Menu name</label>
            <input name="name" oninput="updateMenuName(this.value)" required />
        </div>
        <ul id="menu-items"></ul>
        <button>Create</button>
    </form>
`

    const mainSectionInModal = document.querySelector('.modal-bg > article > main')
    mainSectionInModal.innerHTML = createForm
    console.log(restaurant_id)
}

function displayMenus(index) {
    state.selectedRestaurant = data[index]
    render()
}
function displayRestaurant(index) {
    state.restaurantOpen = data[index].restaurant
    render()
}

function addRestaurant(HTMLform) {
    const form = new FormData(HTMLform)
    const name = form.get(`name`)
    const imageURL = form.get(`imageURL`)
    fetch(`/restaurants`, {
        method: `POST`,
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ name, imageURL })
    })
        .then(res => res.json())
        .then(restaurant => {
            data.push(restaurant)
        
        })
        .catch(console.error)
}

function updateRestaurant(HTMLform) {
    const form = new FormData(HTMLform)
    const id = form.get(`id`)
    const name = form.get(`name`)
    const imageURL = form.get(`imageURL`)
    fetch(`/restaurants/${id}`, {
        method: `PUT`,
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ name, imageURL })
    })
        .then(res => res.json())
        .then(restaurant => {
            const index = data.findIndex(restaurant => {
                return restaurant.id.toString() === id.toString()
            })
            data[index] = restaurant
            render()
        })
        .catch(console.error)
}

function deleteRestaurant(HTMLform) {
    const form = new FormData(HTMLform)
    const id = form.get(`id`)
    fetch(`/restaurants/${id}`, {
        method: `DELETE`,
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(res => res.json())
    .then(restaurant => {
            const index = data.findIndex(restaurant => {
                return restaurant.id.toString() === id.toString()
            })
            data.splice(index, 1)
            render()
        })
        .catch(console.error)
}

function closeModal() {
    state.selectedRestaurant = null
    render()
}

function openAccordian() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
}


//function deleteRest() {
//}