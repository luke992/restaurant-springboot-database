package io.barclays.httpserver;

import javax.persistence.Entity;

import java.util.List;

import javax.persistence.*;

@Entity
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String title;
    @ManyToOne
    private Restaurant restaurant;

   
    @JoinColumn(name = "menu_id")
    @OneToMany(fetch = FetchType.EAGER)
    private List<Item> items;


    public List<Item> getItems() {
        return items;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    

}
