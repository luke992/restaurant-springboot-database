package io.barclays.httpserver;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ItemController {
    private ItemRepository repo;
    private MenuRepository menuRepo;

    public ItemController(ItemRepository repo, MenuRepository menuRepo) {
        this.repo = repo;
        this.menuRepo = menuRepo;
    }

    @PostMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items")
    public Item addMenu(@RequestBody Item ItemData, @PathVariable Integer menu_id) {
        Menu menu = menuRepo.findById(menu_id).get();
        ItemData.setMenu(menu);
        return repo.save(ItemData);
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{id}")
    public void deleteOne(@PathVariable Integer restaurant_id, @PathVariable Integer menu_id, @PathVariable Integer id) {
        repo.deleteById(id);
    }

    @PutMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{id}")
    public Item updateOne(@RequestBody Item itemUpdate, @PathVariable Integer restaurant_id, @PathVariable Integer menu_id, @PathVariable Integer id) {
        Optional<Item> optionalItem = repo.findById(id);
        try {
            Item item = optionalItem.get();
            item.setItemName(itemUpdate.getItemName());
            item.setPrice(itemUpdate.getPrice());
            return repo.save(item);

        } catch (Exception err) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
        }
    }

}
